#include "paraGraph.hh"


int paraGraph::getN() const {
    return n;
}

paraGraph::paraGraph(int n, int m) : n(n), m(m) {}



using namespace std;

void printPath(int* parent, int j, ostream& o)
{
    // Base Case : If j is source
    if (parent[j] == -1)
        return;

    printPath(parent, parent[j], o);
    o << j << " ";
}


int paraGraph::dijkstra(ostream& o, int start, int stop)
{

    int visited_list[getN()]; //maskymalnie N wiercholkow
    int a = 0; // incrementator
    int* parent = new int[getN()];
    for(int krzak=0; krzak<getN(); krzak++){
        parent[krzak] = -1;
    }
    int* koszty = new int[getN()];
    for(int z=0; z<(getN()); z++){
        koszty[z] = INT_MAX;
    }

    priority_kju q;

    q.enqueue(0, start);
    koszty[start]=0;
    while(!q.empty())
    {
        int wierzch=q.top().getKoszta();
        int koszt=q.top().getInd_wierz();
        /*visited_list[a] = wierzch;
        a++;*/
        q.dequeue();
        for(int i=0; i<get_adjecent(wierzch).size(); i++)
        {
            int nw=get_adjecent(wierzch)[i].getInd_wierz();
            int e=get_adjecent(wierzch)[i].getKoszta();
            if(e-koszt<koszty[nw])
            {
                q.enqueue(koszt-e, nw);
                koszty[nw]=e-koszt;
                parent[nw] = wierzch;
            }
        }
    }
    int zwracam = koszty[stop];
    if(koszty[stop] == INT_MAX){
        o << "nie ma sciezki" << " || ";
    }
    else{
        o << zwracam << " || ";
    }
    o << start << " ";
    printPath(parent, stop, o);
    /*for(int x=0;x<a; x++){
        cout << visited_list[x] << " ";
    }*/
    o << endl;

    delete[] koszty;
    delete[] parent;
    return zwracam;
}

paraGraph::~paraGraph() = default;
