#ifndef PARA_HH
#define PARA_HH


class para {
public:
    para(int ind_wierz, int koszta);

    para();

    int getInd_wierz() const;

    int getKoszta() const;

private:
    int ind_wierz, koszta;
};


#endif //PROJO2_PARA_HH
