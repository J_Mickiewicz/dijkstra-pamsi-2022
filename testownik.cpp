#include "testownik.hh"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ctime>


using namespace std;


void testownik::generuj(int l_wie, int gestosc) {
    std::ofstream myfile;
    myfile.open("graf.txt");
    long long l_kraw = l_wie * (l_wie - 1) * gestosc / 200;// /2*100
    myfile << l_kraw << " " << l_wie << " " << rand() % l_wie << std::endl;
    Adjacency_matrix temp(l_wie, l_kraw);
    int from, to, cost;
    for (long long i = 0; i < l_kraw; i++) {
        from = rand() % (l_wie);
        to = rand() % (l_wie);

        while (to == from || temp.get_edge(from, to) != -1) {
            from = rand() % (l_wie);
            to = rand() % (l_wie);
        }
        cost = (rand() % 20) + 1; // random cost for the edge
        temp.create_edge(from, to, cost);
        myfile << from << " " << to << " " << cost << std::endl;

    }
    myfile.close();
}

Adjacency_matrix testownik::wczytaj_matryce() {
    std::ifstream myfile;
    myfile.open("graf.txt");
    long long m;
    int n, v1, v2, cost;
    myfile >> m >> n >> start;
    Adjacency_matrix Matryca(n, m);
    for (long long i = 0; i < m; i++) {
        myfile >> v1 >> v2 >> cost;
        Matryca.create_edge(v1, v2, cost);
    }
    myfile.close();
    return Matryca;
}

Adjacency_List testownik::wczytaj_liste() {
    std::ifstream myfile;
    myfile.open("graf.txt");
    long long m;
    int n, v1, v2, cost;
    myfile >> m >> n >> start;
    Adjacency_List Listunia(n, m);
    for (long long i = 0; i < m; i++) {
        myfile >> v1 >> v2 >> cost;
        Listunia.create_edge(v1, v2, cost);
    }
    myfile.close();
    return Listunia;
}

void testownik::testuj() {
    std::ofstream wyniki;
    std::ofstream czas;
    wyniki.open("wynik.txt");
    czas.open("czasy.txt");
    int ilosc_wierzcholkow[5] = {5, 50, 100, 150,200};
    int gestosci[4] = {25, 50, 75, 100};
    int przypadki[4] = {100, 50, 25, 15};
    clock_t start_t, end_t;
    double total_t;
    int x = 0;
    //for od implementacji grafu
    for (int i = 1; i < 2; i++) {
        //for od ilosci wierzcholkow
        for (int j = 0; j < 5; j++) {
            //for od gestosci
            for (int k = 0; k < 4; k++) {
                if (i == 0) {//wtedy matrix
                    if (j == 0 || j == 1) { x = 0; }
                    else if (j == 2) { x = 1; }
                    else if (j == 3) { x = 2; }
                    else if (j == 4) { x = 3; }
                    czas << "MATRIX" << " wierzcholki: " << ilosc_wierzcholkow[j] << " gestosc: " << gestosci[k]
                         << " ilosc przypadkow testowych: " << przypadki[x] << std::endl;
                    wyniki << "MATRIX" << " wierzcholki: " << ilosc_wierzcholkow[j] << " gestosc: " << gestosci[k]
                           << std::endl;
                    for (int l = 0; l < przypadki[x]; l++) {
                        generuj(ilosc_wierzcholkow[j], gestosci[k]);
                        Adjacency_matrix Matryca = wczytaj_matryce();
                        start_t = clock();
                        for (int p = 0; p < ilosc_wierzcholkow[j]; p++) {
                            if (p == start) {
                                continue;
                            }
                            Matryca.dijkstra(wyniki, start, p);
                            wyniki << std::endl;

                        }
                        end_t = clock();
                        total_t = (double) (end_t - start_t) / CLOCKS_PER_SEC;
                        czas << total_t << std::endl;
                    }
                } else {//wtedy lista
                    if (j == 0 || j == 1) { x = 0; }
                    else if (j == 2) { x = 1; }
                    else if (j == 3) { x = 2; }
                    else if (j == 4) { x = 3; }
                    czas << endl << endl << "LISTA" << " wierzcholki: " << ilosc_wierzcholkow[j] << " gestosc: "
                         << gestosci[k] << " ilosc przypadkow testowych: " << przypadki[x] << std::endl;
                    wyniki << endl << endl << "LISTA" << " wierzcholki: " << ilosc_wierzcholkow[j] << " gestosc: "
                           << gestosci[k] << std::endl;
                    for (int l = 0; l < przypadki[x]; l++) {
                        generuj(ilosc_wierzcholkow[j], gestosci[k]);
                        Adjacency_List Lista = wczytaj_liste();
                        start_t = clock();
                        for (int p = 0; p < ilosc_wierzcholkow[j]; p++) {
                            if (p == start) {
                                continue;
                            }
                            Lista.dijkstra(wyniki, start, p);
                            wyniki << std::endl;

                        }
                        end_t = clock();
                        total_t = (double) (end_t - start_t) / CLOCKS_PER_SEC;
                        czas << total_t << std::endl;
                    }
                }

            }

        }

    }
    czas.close();
    wyniki.close();
}