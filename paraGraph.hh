#ifndef PARAGRAPH_HH
#define PARAGRAPH_HH
#include<bits/stdc++.h>
#include "para.hh"
#include "priority_kju.hh"
#include "retarded_vector.hh"
#include "paraGraph.hh"

class paraGraph {
private:

protected:
    int n; //ilosc wiercholkow
    int m; //ilosc krawedzi

public:
    paraGraph(int n, int m);

    virtual retarded_vector<para> get_adjecent(int present_vertex) = 0;
    int getN() const;
    int dijkstra(std::ostream& o, int start, int stop);

    virtual ~paraGraph() = 0;
};


#endif //PARAGRAPH_HH
