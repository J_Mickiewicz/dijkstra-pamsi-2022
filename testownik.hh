#ifndef TESTOWNIK_HH
#define TESTOWNIK_HH
#include "Adjacency_List.hh"
#include "Adjacency_matrix.hh"

class testownik {
public:
    void generuj(int l_wie, int gestosc);

    Adjacency_matrix wczytaj_matryce();

    Adjacency_List wczytaj_liste();

    void testuj();

    int start;

private:


};


#endif
