#include "para.hh"

para::para(int ind_wierz, int koszta) : ind_wierz(ind_wierz), koszta(koszta) {}

int para::getInd_wierz() const {
    return ind_wierz;
}

int para::getKoszta() const {
    return koszta;
}

para::para() {
    ind_wierz = 0;
    koszta = 0;
}
