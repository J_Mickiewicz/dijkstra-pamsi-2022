#include "priority_kju.hh"
#include "limits.h"


// Function to insert a new element
// into priority queue
void priority_kju::enqueue( int value, int priority)
{
    // Increase the size
    size++;

    // Insert the element
    buffer[size] = {value, priority};
//    buffer[size].ind_wierz = value;
//    buffer[size].koszta = priority;
}

bool priority_kju::empty() const{
    if(size==(-1)){
        return true;
    }
    else{
        return false;
    }
}

// Function to check the top element
int priority_kju::peek()
{
    int highestPriority = INT_MIN;
    int ind = -1;

    // Check for the element with
    // highest priority
    for (int i = 0; i <= size; i++) {

        // If priority is same choose
        // the element with the
        // highest value
        if (highestPriority == buffer[i].getKoszta() && ind > -1
            && buffer[ind].getInd_wierz() < buffer[i].getInd_wierz()) {
            highestPriority = buffer[i].getKoszta();
            ind = i;
        }
        else if (highestPriority < buffer[i].getKoszta()) {
            highestPriority = buffer[i].getKoszta();
            ind = i;
        }
    }

    // Return position of the element
    return ind;
}

para priority_kju::top(){
    int index = peek();
    return buffer[index];
}

// Function to remove the element with
// the highest priority
void priority_kju::dequeue()
{
    // Find the position of the element
    // with highest priority
    int ind = peek();

    // Shift the element one index before
    // from the position of the element
    // with highest priority is found
    for (int i = ind; i < size; i++) {
        buffer[i] = buffer[i + 1];
    }

    // Decrease the size of the
    // priority queue by one
    size--;
}

priority_kju::priority_kju() {
    buffer = new para[1000];
}

priority_kju::~priority_kju() {
    delete[] buffer;
}
