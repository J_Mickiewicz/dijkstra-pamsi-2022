#include "Adjacency_matrix.hh"
#include <iostream>

Adjacency_matrix::Adjacency_matrix(int n, int m) : paraGraph(n, m) {
    mat = new int*[n];
    for(int i=0; i<n; i++){
        mat[i] = new int[n];
    }
    for(int a=0; a<n; a++){
        for(int b=0; b<n; b++){
            mat[a][b] = -1;
        }
    }
}

//tworzy te znana przegladarke, polecam(chyba mamy konkurencje kto lepsza xD)
void Adjacency_matrix::create_edge(int v1, int v2, int cost){
    mat[v1][v2] = cost;
    mat[v2][v1] = cost;
}

int Adjacency_matrix::get_edge(int v1, int v2){
    return mat[v1][v2];
}

void Adjacency_matrix::create_graph(){
    int v1, v2, cost;
    for(int i=0; i<m; i++)
    {
        std::cout << "podawaj wierzcholek 1, wierzcholek 2 i koszt" << std::endl;
        std::cin >> v1 >> v2 >> cost; //v1 - vertex1 v2-vertex2 cost-route cost
        create_edge(v1, v2, cost);
    }
}

retarded_vector<para> Adjacency_matrix::get_adjecent(int present_vertex){
    retarded_vector<para> git;
    for(int i=0; i<n; i++){
        if(mat[present_vertex][i]!=(-1)) {
            git.push_back({i, mat[present_vertex][i]});
        }
    }
    return git;
}

Adjacency_matrix::~Adjacency_matrix() {
    for(int i=0; i<n; i++){
        delete[] mat[i];
    }
    delete[] mat;
}

