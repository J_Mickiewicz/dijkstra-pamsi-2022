#ifndef ADJACENCY_LIST_H
#define ADJACENCY_LIST_H
#include "paraGraph.hh"


class Adjacency_List:public paraGraph {
private:
    retarded_vector<para>* w;
public:
    explicit Adjacency_List(int n, int m);

    virtual ~Adjacency_List();

    retarded_vector<para> get_adjecent(int present_vertex) override;

    void create_graph();

    void create_edge(int v1, int v2, int cost);
};


#endif //ADJACENCY_LIST_H
