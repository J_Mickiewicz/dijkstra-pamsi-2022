#ifndef ADJECENCY_MATRIX_HH
#define ADJECENCY_MATRIX_HH
#include "paraGraph.hh"

class Adjacency_matrix: public paraGraph {
private:
    int** mat;

public:
    explicit Adjacency_matrix(int n, int m);

    void create_edge(int v1, int v2, int cost);

    int get_edge(int v1, int v2);

    void create_graph();

    retarded_vector<para> get_adjecent(int present_vertex) override;

    virtual ~Adjacency_matrix();
};


#endif //ADJECENCY_MATRIX_HH
