#ifndef PRIORITY_KJU_HH
#define PRIORITY_KJU_HH
#include "para.hh"

class priority_kju {
private:
    para* buffer;
    // Pointer to the last index
    int size = -1;
public:
    priority_kju();

    void enqueue( int value, int priority);
    int peek();
    void dequeue();
    bool empty() const;
    para top();

    virtual ~priority_kju();
};


#endif //PROJO2_PRIORITY_KJU_HH
