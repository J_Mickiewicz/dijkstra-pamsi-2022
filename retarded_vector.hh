#ifndef RETARDED_VECTOR_HH
#define RETARDED_VECTOR_HH


template<typename T>
class retarded_vector {
private:
    T* arr;
    int last_index = -1;
public:
    explicit retarded_vector(int n) {
        arr = new T[n];
    }

    retarded_vector() {
        arr = new T[1001];
    }

    void push_back(T smth){
        arr[++last_index] = smth;
    }

    void pop_back(){
        last_index--;
    }

    int size(){
        return last_index+1;
    }

    T& operator[](int index){
        return arr[index];
    }

    virtual ~retarded_vector() {
        delete[] arr;
    }

};


#endif //PROJO2_RETARDED_VECTOR_HH
